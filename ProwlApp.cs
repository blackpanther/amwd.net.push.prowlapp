﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace AMWD.Net.Push
{
	/// <summary>
	/// Implements the API reference of prowlapp.com
	/// </summary>
	public class ProwlApp : IDisposable
	{
		#region Fields

		private static readonly string BaseUrl = "https://api.prowlapp.com";
		private static readonly string Version = "publicapi";

		private readonly XmlReaderSettings XmlReaderSettings = new XmlReaderSettings
		{
			Async = true,
			IgnoreComments = true,
			IgnoreWhitespace = true
		};

		private HttpClient client;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ProwlApp"/> class.
		/// </summary>
		public ProwlApp()
		{
			client = new HttpClient
			{
				BaseAddress = new Uri(BaseUrl)
			};
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProwlApp"/> class.
		/// </summary>
		/// <param name="apiKey">The API key.</param>
		public ProwlApp(string apiKey)
			: this()
		{
			ApiKey = apiKey;
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// Gets or sets the API key (required).
		/// </summary>
		public string ApiKey { get; set; }

		/// <summary>
		/// Gets or sets the provider key (lenght: 40 chars).
		/// </summary>
		public string ProviderKey { get; set; }

		/// <summary>
		/// Gets or sets the application name (max. 256 chars).
		/// </summary>
		public string Application { get; set; }

		/// <summary>
		/// Gets or sets the subject (max. 1024 chars).
		/// </summary>
		public string Subject { get; set; }

		/// <summary>
		/// Gets or sets the url (max. 512 chars).
		/// </summary>
		public string Url { get; set; }

		/// <summary>
		/// Gets or sets the message (max. 10000 chars).
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the priority (default: <see cref="Priorities.Normal"/>).
		/// </summary>
		public Priorities Priority { get; set; } = Priorities.Normal;

		/// <summary>
		/// Gets the remaining number of allowed requests until <see cref="ResetAt"/>.
		/// </summary>
		public int? RemainingRequests { get; private set; }

		/// <summary>
		/// Gets the time when the request count is reset.
		/// </summary>
		public DateTime? ResetAt { get; private set; }

		#endregion Properties

		#region Public methods

		/// <summary>
		/// Sends the message.
		/// </summary>
		/// <returns>true on success, otherwise false.</returns>
		public async Task<bool> Send(CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(ApiKey))
			{
				throw new ArgumentNullException(nameof(ApiKey));
			}
			if (string.IsNullOrWhiteSpace(Application))
			{
				throw new ArgumentNullException(nameof(Application));
			}
			if (string.IsNullOrWhiteSpace(Subject) && string.IsNullOrWhiteSpace(Message))
			{
				throw new ArgumentNullException($"{nameof(Subject)} or {nameof(Message)} is needed");
			}

			var param = new Dictionary<string, string>
			{
				{ "apikey", ApiKey },
				{ "priority", ((int)Priority).ToString() },
				{ "application", Application }
			};

			if (!string.IsNullOrWhiteSpace(ProviderKey))
			{
				param.Add("providerkey", ProviderKey);
			}
			if (!string.IsNullOrWhiteSpace(Url))
			{
				param.Add("url", Url);
			}
			if (!string.IsNullOrWhiteSpace(Subject))
			{
				param.Add("event", Subject);
			}
			if (!string.IsNullOrWhiteSpace(Message))
			{
				param.Add("description", Message);
			}

			var response = await Request(RequestMethods.Post, "/add", param, cancellationToken);

			var success = response.SingleOrDefault(n => n.Name.ToLower() == "success");
			var error = response.SingleOrDefault(n => n.Name.ToLower() == "error");

			if (success != null)
			{
				RemainingRequests = int.Parse(success.Attributes["remaining"]);
				var resetUnix = int.Parse(success.Attributes["resetdate"]);
				ResetAt = FromUnixTimestamp(resetUnix);

				return true;
			}
			if (error != null)
			{
				throw new HttpRequestException($"Sending notification failed: {error.Attributes["code"]} - {error.Value}");
			}

			return false;
		}

		/// <summary>
		/// Verifies whether the API key is valid.
		/// </summary>
		/// <param name="cancellationToken"></param>
		public async Task<bool> Verify(CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(ApiKey))
			{
				throw new ArgumentNullException(nameof(ApiKey));
			}

			var param = new Dictionary<string, string>
			{
				{ "apikey", ApiKey }
			};

			if (!string.IsNullOrWhiteSpace(ProviderKey))
			{
				param.Add("providerkey", ProviderKey);
			}

			var response = await Request(RequestMethods.Post, "/verify", param, cancellationToken);

			var success = response.SingleOrDefault(n => n.Name.ToLower() == "success");

			if (success != null)
			{
				RemainingRequests = int.Parse(success.Attributes["remaining"]);
				var resetUnix = int.Parse(success.Attributes["resetdate"]);
				ResetAt = FromUnixTimestamp(resetUnix);

				return true;
			}

			return false;
		}

		/// <summary>
		/// Retrieves a token using the provider key to create a new api key.
		/// </summary>
		/// <param name="cancellationToken"></param>
		public async Task<Dictionary<string, string>> GetToken(CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(ProviderKey))
			{
				throw new ArgumentNullException(ProviderKey);
			}

			var param = new Dictionary<string, string>
			{
				{ "providerkey", ProviderKey }
			};

			var response = await Request(RequestMethods.Post, "/retrieve/token", param, cancellationToken);
			var success = response.SingleOrDefault(n => n.Name.ToLower() == "success");
			var retrieve = response.SingleOrDefault(n => n.Name.ToLower() == "retrieve");
			var error = response.SingleOrDefault(n => n.Name.ToLower() == "error");

			string token = null;
			string url = null;

			if (success != null && retrieve != null)
			{
				RemainingRequests = int.Parse(success.Attributes["remaining"]);
				var resetUnix = int.Parse(success.Attributes["resetdate"]);
				ResetAt = FromUnixTimestamp(resetUnix);

				token = retrieve.Attributes["token"];
				url = retrieve.Attributes["url"];
			}
			if (error != null)
			{
				throw new HttpRequestException($"Retrieving token failed: {error.Attributes["code"]} - {error.Value}");
			}

			return new Dictionary<string, string>
			{
				{ "token", token },
				{ "url", url }
			};
		}

		/// <summary>
		/// Retrieves a new API key using the provider key and previously retrieved token.
		/// </summary>
		/// <param name="token">The retrieved token.</param>
		/// <param name="apply">A value indicating whether to apply the api key to the current instance (default: true).</param>
		/// <param name="cancellationToken"></param>
		public async Task<string> GetApiKey(string token, bool apply = true, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(ProviderKey))
			{
				throw new ArgumentNullException(ProviderKey);
			}
			if (string.IsNullOrWhiteSpace(token))
			{
				throw new ArgumentNullException(token);
			}

			var param = new Dictionary<string, string>
			{
				{ "providerkey", ProviderKey },
				{ "token", token }
			};

			var response = await Request(RequestMethods.Post, "/retrieve/apikey", param, cancellationToken);
			var success = response.SingleOrDefault(n => n.Name.ToLower() == "success");
			var retrieve = response.SingleOrDefault(n => n.Name.ToLower() == "retrieve");
			var error = response.SingleOrDefault(n => n.Name.ToLower() == "error");

			string apiKey = null;
			if (success != null && retrieve != null)
			{
				RemainingRequests = int.Parse(success.Attributes["remaining"]);
				var resetUnix = int.Parse(success.Attributes["resetdate"]);
				ResetAt = FromUnixTimestamp(resetUnix);

				apiKey = retrieve.Attributes["apikey"];

				if (apply)
				{
					ApiKey = apiKey;
				}
			}
			if (error != null)
			{
				throw new HttpRequestException($"Retrieving token failed: {error.Attributes["code"]} - {error.Value}");
			}

			return apiKey;
		}

		#endregion Public methods

		#region Private methods

		private async Task<List<XmlNode>> Request(RequestMethods method, string path, Dictionary<string, string> param = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrWhiteSpace(path))
			{
				throw new ArgumentNullException(nameof(path));
			}

			HttpResponseMessage response = null;
			switch (method)
			{
				case RequestMethods.Get:
					var queryString = "";
					if (param?.Any() == true)
					{
						queryString = "?" + string.Join("&", param.Select(kvp => kvp.Key + "=" + kvp.Value));
					}
					response = await client.GetAsync($"/{Version}/{path.Trim('/')}{queryString}", cancellationToken);
					break;
				case RequestMethods.Post:
					if (param == null)
					{
						param = new Dictionary<string, string>();
					}
					response = await client.PostAsync($"/{Version}/{path.Trim('/')}", new FormUrlEncodedContent(param), cancellationToken);
					break;
				default:
					throw new NotSupportedException($"Request method '{method}' not known");
			}
			return ParseResponse(await response.Content.ReadAsStringAsync());
		}

		private DateTime FromUnixTimestamp(int unix)
		{
			return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
				.AddSeconds(unix);
		}

		private List<XmlNode> ParseResponse(string response)
		{
			var list = new List<XmlNode>();
			var read = false;

			using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(response)))
			using (var xml = XmlReader.Create(stream, XmlReaderSettings))
			{
				while (xml.Read())
				{
					if (xml.NodeType == XmlNodeType.Element)
					{
						if (xml.LocalName.ToLower() == "prowl")
						{
							read = true;
						}
						else if (read)
						{
							var node = new XmlNode
							{
								Name = xml.LocalName,
								Value = xml.HasValue ? xml.Value : null
							};

							if (xml.HasAttributes)
							{
								while (xml.MoveToNextAttribute())
								{
									node.Attributes.Add(xml.Name, xml.Value);
								}
							}

							list.Add(node);
						}
					}
					else if (xml.NodeType == XmlNodeType.EndElement && xml.LocalName.ToLower() == "prowl")
					{
						read = false;
					}
				}
			}

			return list;
		}

		#endregion Private methods

		#region IDisposable implementation

		private bool isDisposed;

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
		}

		private void Dispose(bool disposing)
		{
			if (isDisposed)
			{
				return;
			}

			isDisposed = true;

			client?.Dispose();
			client = null;
		}

		#endregion IDisposable implementation

		#region Enums

		/// <summary>
		/// Represents the possible priorities for an prowl message.
		/// </summary>
		public enum Priorities
		{
			/// <summary>
			/// Normal priority.
			/// </summary>
			Normal = 0,
			/// <summary>
			/// Lower priority.
			/// </summary>
			Lower = -1,
			/// <summary>
			/// Lowest priority.
			/// </summary>
			Lowest = -2,
			/// <summary>
			/// Higher priority.
			/// </summary>
			Higher = 1,
			/// <summary>
			/// Highest priority. Message bypasses quiet hours (app settings).
			/// </summary>
			Highest = 2
		}

		private enum RequestMethods
		{
			Get,
			Post
		}

		#endregion Enums

		#region Classes

		private class XmlNode
		{
			public string Name { get; set; }

			public string Value { get; set; }

			public Dictionary<string, string> Attributes { get; } = new Dictionary<string, string>();
		}

		#endregion Classes
	}
}
